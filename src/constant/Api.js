import React, { Component } from 'react'
import axios from 'axios'

const API_URL = process.env.REACT_APP_API;
const HEAD = {
    headers: {
    "Content-Type": "application/json",
    "authorization": localStorage.getItem('token'),
    "access-control-allow-origin": "*",
    'access-control-allow-credentials':true
    }
}

axios.interceptors.response.use(response => {
  return response;
  	}, error => {
	if (error.response.status === 401) {
		localStorage.removeItem('token');
			window.location.href = '/login'
	}
	return error;
  });

export default {
    login(data) {
      return new Promise((resolve, reject) => {
        axios
        .post(`${API_URL}/auth`, data, HEAD)
        .then((response) => {
          const { data } = response;
          resolve(response || []);
        })
        .catch((error) => {
          reject(error);
        });
      });
    },
    createUsers(data) {
        return new Promise((resolve, reject) => {
          axios
          .post(`${API_URL}/users`, data, HEAD)
          .then((response) => {
            const { data } = response;
            resolve(data || []);
          })
          .catch((error) => {
            reject(error);
          });
        });
    },
    getUsers() {
        return new Promise((resolve, reject) => {
          axios
          .get(`${API_URL}/users`, HEAD)
          .then((response) => {
            const { data } = response;
            resolve(data || []);
          })
          .catch((error) => {
            reject(error);
          });
        });
    },
    getDetailUsers(id) {
        return new Promise((resolve, reject) => {
          axios
          .get(`${API_URL}/users/${id}`, HEAD)
          .then((response) => {
            const { data } = response;
            resolve(data || []);
          })
          .catch((error) => {
            reject(error);
          });
        });
    },
    updateUsers(id, data) {
      return new Promise((resolve, reject) => {
        axios
        .put(`${API_URL}/users/${id}`, data, HEAD)
        .then((response) => {
          const { data } = response;
          resolve(data || []);
        })
        .catch((error) => {
          reject(error);
        });
      });
    },
    deleteUsers(id) {
      return new Promise((resolve, reject) => {
        axios
        .delete(`${API_URL}/users/${id}`, HEAD)
        .then((response) => {
          const { data } = response;
          resolve(data || []);
        })
        .catch((error) => {
          reject(error);
        });
      });
    },
}