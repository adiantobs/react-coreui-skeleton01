import React, { useState }  from 'react'
import { Link, useHistory } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Api from 'src/constant/Api'
import axios from 'axios'

const Login = () => {
  const history = useHistory();
  const [ username, setUsername ] = useState('')
  const [ password, setPassword ] = useState('')
  const [ validated, setValidated ] = useState(false)

  const { login } = Api
  const onSubmit = (e) => {
    const form = e.currentTarget 
    if(form.checkValidity() === false) {
        e.preventDefault()
        e.stopPropagation()
    }else{
        e.preventDefault()
        onCreate()
        setValidated(true)
    }
        
  }

  const onCreate = async() => {
    let data = {
    "username": username,
    "password": password
    }
    login(data)
    .then((res) => {
      console.log(res, 'res')
      let result = res?.response?.data?.message
      window.alert(result)
      localStorage.setItem('token', res.data.access_token)
      window.location.href = '/dashboard'
    })
    .catch((err) => {
        console.log(err)
        window.alert(err.message)
    })

    // contoh satuan
    // try {
    //   await axios.post("http://127.0.0.1:3004/auth", data)
    //   console.log('test')
    // } catch(e) {

    //   console.log('er', e)
    // }
  }

  // console.log(username, 'username')
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={onSubmit}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" placeholder="Username" value={username} onChange={(e) => (setUsername(e.target.value))} required autoComplete="username" />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" placeholder="Password" required value={password} onChange={(e) => (setPassword(e.target.value))} autoComplete="current-password" />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" type="submit" className="px-4">Login</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Forgot password?</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                      labore et dolore magna aliqua.</p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
