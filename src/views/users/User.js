import React, { useEffect, useState } from 'react'
import { CCard, CCardBody, CCardHeader, CButton, CCol, CForm, CInput, CLabel, CRow, CSelect } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { Input } from 'reactstrap'
import usersData from './UsersData'
import Api from 'src/constant/Api'

const User = ({match}) => {
  const user = usersData.find( user => user.id.toString() === match.params.id)
  const userDetails = user ? Object.entries(user) : 
    [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]
  const { getDetailUsers } = Api
  const [ userDat, setUserDat ] = useState('')
  const [ dataMap, setDataMap ] = useState('')
  const [ validated, setValidated ] = useState(false)
  const [ username, setUsername ] = useState('')
  const [ password, setPassword ] = useState('')
  const [ first, setFirst ] = useState('')
  const [ last, setLast ] = useState('')
  const [ address, setAddress ] = useState('')
  const [ grad, setGrad ] = useState('')

  const { updateUsers } = Api

  const getDetail = () => {
    getDetailUsers(match.params.id)
    .then((res) => {
      setUserDat(res.users)      
      setUsername(res.users.username)
      setPassword(res.users.password)
      setFirst(res.users.profile.first_name)
      setLast(res.users.profile.last_name)
      setAddress(res.users.profile.address)
      setGrad(res.users.profile.graduation)
    })
    .catch((err) => {

    })
  }

  useEffect(() => {
    getDetail()
  }, []);

  const onSubmit = (e) => {
    const form = e.currentTarget 
    if(form.checkValidity() === false) {
        e.preventDefault()
        e.stopPropagation()
    }else{
        e.preventDefault()
        onUpdate()
        setValidated(true)
    }
        
  }

  const onUpdate = () => {
    let data = {
      "profiles": {
        "first_name": first,
        "last_name": last,
        "address": address,
        "graduation": grad
    },
    "username": username,
    "password": password
    }
    updateUsers(match.params.id, data)
    .then((res) => {
      window.alert('data berhasil diupdate')
    })
    .catch((err) => {
        console.log(err)
    })
  }

  return (
    <CRow>
      <CCol lg={6}>
        <CCard>
          <CCardHeader>
            User id: {match.params.id}
          </CCardHeader>
          <CCardBody>
          {!userDat ? <span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span> : 
              <span>
                <CForm
                  onSubmit={onSubmit}
                  className='needs-validation'
                  >
                  <CRow className="mb-3">
                      <CCol xl={12}>
                          <CLabel>Username</CLabel>
                          <CInput 
                            // style={{backgroundColor: 'green'}}
                            // className={'form-khusus'}
                            type="text"
                            defaultValue={username}
                            onChange={(e) => (setUsername(e.target.value))}
                            >
                          </CInput>
                      </CCol>
                  </CRow>
                  <CRow className="mb-3">
                      <CCol xl={12}>
                          <CLabel>Password</CLabel>
                          <Input
                            type="password"
                            defaultValue={password}
                            onChange={(e) => (setPassword(e.target.value))}
                            >
                          </Input>
                      </CCol>
                  </CRow>
                  <CRow className="mb-3">
                      <CCol xl={6}>
                          <CLabel>First Name</CLabel>
                          <CInput 
                            type="text"
                            value={first}
                            onChange={(e) => (setFirst(e.target.value))}
                            >
                          </CInput>
                      </CCol>
                      <CCol xl={6}>
                          <CLabel>Last Name</CLabel>
                          <Input 
                            type="text"
                            value={last}
                            onChange={(e) => (setLast(e.target.value))}
                            >
                          </Input>
                      </CCol>
                  </CRow>
                  <CRow className="mb-3">
                      <CCol xl={12}>
                          <CLabel>Address</CLabel>
                          <textarea className='form-control' 
                          type="text"
                          value={address}
                          onChange={(e) => (setAddress(e.target.value))}
                          />
                      </CCol>
                  </CRow>
                  <CRow className="mb-3">
                      <CCol xl={6}>
                          <CLabel>Graduation</CLabel>
                          <CSelect value={grad}
                            onChange={(e) => (setGrad(e.target.value))}
                          >
                              <option value={'yes'}>yes</option>
                              <option value={'no'}>no</option>
                              <option value={'maybe'}>maybe</option>
                              <option value={'astaga'}>astaga</option>
                          </CSelect>
                      </CCol>
                  </CRow>
                  <CRow className="mb-3">
                      <CButton
                        className='pull-right'
                        color="primary"
                        size="sm"
                        type='submit'>
                          Update
                      </CButton>
                  </CRow>
                </CForm>
              </span>
          }
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default User
