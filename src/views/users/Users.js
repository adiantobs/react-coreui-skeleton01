import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
} from '@coreui/react'

import Api from 'src/constant/Api'

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}

const Users = () => {
  const [usersData, setUsersData] = useState([]);
  const { getUsers, deleteUsers } = Api

  const getData = () => {
    getUsers()
    .then((res) => {
      setUsersData(res.users)
    })
    .catch((err) => {
      console.log(err, 'errornya apa')
    })
  }
  useEffect(() => {
    // untuk load data awal
    getData()
  }, []);
  const history = useHistory() //([0-9]+)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage ? currentPage : 1)
  const total = usersData ? Math.ceil(usersData.length / 5) : 0;
  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/users?page=${newPage}`)
  }

  useEffect(() => {
    currentPage !== page && (currentPage === 0) ? setPage(1) : setPage(currentPage)
  }, [currentPage, page])

  const deleteUser = (id) => {
    deleteUsers(id)
    .then((res) => {
      getData()
    })
    .catch((err) => {

    })
  }

  return (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            Users
            <small className="text-muted"> example</small>
            <CButton className="btn-info ml-3" onClick={() => history.push(`/addusers`)}>Add User</CButton>  
          </CCardHeader>
          <CCardBody>
          <CDataTable
          style={{'widht': '1000px'}}
            items={usersData}
            fields={[
              { key: 
              'username', _classes: 'font-weight-bold' },
              'createdAt', 
              'role', 
              'action'
            ]}
            hover
            striped
            itemsPerPage={5}
            activePage={page}
            clickableRows
            onRowClick={(item) => history.push(`/users/${item.id}`)}
            scopedSlots = {{
              'action':
                (item)=>(
                  <td>
                  <CButton
                    className="btn-success mr-2"
                    onClick={() => history.push(`/users/${item.id}`)}
                    >
                    View
                  </CButton>
                  <CButton
                    className="btn-danger"
                    onClick={() => deleteUser(item.id)}>
                    Delete
                  </CButton>
                    {/* <CBadge color={getBadge(item.status)}>
                      {item.status}
                    </CBadge> */}
                  </td>
                )
            }}
          />
          <CPagination
            activePage={page}
            onActivePageChange={pageChange}
            pages={total}
            doubleArrows={false} 
            align="center"
          />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Users
