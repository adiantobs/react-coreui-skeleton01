import React, { useState } from 'react'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CForm, CInput, CLabel, CRow, CSelect } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { Input } from 'reactstrap'
import usersData from './UsersData'
import Api from 'src/constant/Api'
import { useHistory } from "react-router-dom";

const User = ({match}) => {
  const user = usersData.find( user => user.id.toString() === match.params.id)
  const userDetails = user ? Object.entries(user) : 
    [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]
  const history = useHistory();
  const [ validated, setValidated ] = useState(false)
  const [ username, setUsername ] = useState('')
  const [ password, setPassword ] = useState('')
  const [ first, setFirst ] = useState('')
  const [ last, setLast ] = useState('')
  const [ address, setAddress ] = useState('')
  const [ grad, setGrad ] = useState('')

  const { createUsers } = Api
  
  const onSubmit = (e) => {
    const form = e.currentTarget 
    if(form.checkValidity() === false) {
        e.preventDefault()
        e.stopPropagation()
    }else{
        e.preventDefault()
        onCreate()
        setValidated(true)
    }
        
  }

  const onCreate = () => {
    let data = {
      "profiles": {
        "first_name": first,
        "last_name": last,
        "address": address,
        "graduation": grad
    },
    "username": username,
    "password": password
    }
    createUsers(data)
    .then((res) => {
      window.location.href('/users')
    })
    .catch((err) => {
        console.log(err)
    })
  }

  return (
    <CRow>
      <CCol lg={6}>
        <CCard>
          <CCardHeader>
            Add User 
          </CCardHeader>
          <CCardBody>
              <CForm 
                onSubmit={onSubmit}
                className='needs-validation'
                >
                <CRow className="mb-3">
                    <CCol xl={12}>
                        <CLabel>Username</CLabel>
                        <CInput 
                          type="text"
                          name='username'
                          required
                          value={username}
                          onChange={(e) => (setUsername(e.target.value))}
                          >
                        </CInput>
                    </CCol>
                </CRow>
                <CRow className="mb-3">
                    <CCol xl={12}>
                        <CLabel>Password</CLabel>
                        <Input 
                          type="password"
                          name='password'
                          value={password}
                          onChange={(e) => (setPassword(e.target.value))}
                          >
                        </Input>
                    </CCol>
                </CRow>
                <CRow className="mb-3">
                    <CCol xl={6}>
                        <CLabel>First Name</CLabel>
                        <CInput type="text"
                          name='first'
                          value={first}
                          onChange={(e) => (setFirst(e.target.value))}
                          >
                        </CInput>
                    </CCol>
                    <CCol xl={6}>
                        <CLabel>Last Name</CLabel>
                        <Input type="text"
                          name='last'
                          value={last}
                          onChange={(e) => (setLast(e.target.value))}
                          >
                        </Input>
                    </CCol>
                </CRow>
                <CRow className="mb-3">
                    <CCol xl={12}>
                        <CLabel>Address</CLabel>
                        <textarea 
                          className='form-control' 
                          type="text"
                          name='address'
                          value={address}
                          onChange={(e) => (setAddress(e.target.value))}
                        />
                    </CCol>
                </CRow>
                <CRow className="mb-3">
                    <CCol xl={6}>
                        <CLabel>Graduation</CLabel>
                        <CSelect 
                          name='grad'
                          value={grad}
                          onChange={(e) => (setGrad(e.target.value))}
                          options={{label: 'yes', value:'yes'}}
                          >
                            <option>yes</option>
                            <option>no</option>
                            <option>maybe</option>
                            <option>astaga</option>
                        </CSelect>
                    </CCol>
                </CRow>
                <CRow className="mb-3">
                    <CButton
                      className='pull-right'
                      color="primary"
                      size="sm"
                      type='submit'>
                        Save
                    </CButton>
                </CRow>
              </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default User
